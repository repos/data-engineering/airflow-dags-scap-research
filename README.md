# Airflow Dags Scap Research

scap config repo for the 'research' deployment of the airflow-dags repository.

https://wikitech.wikimedia.org/wiki/Analytics/Systems/Airflow
https://doc.wikimedia.org/mw-tools-scap/scap3/repo_config.html
